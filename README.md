**Parking Bill Calculator**

My solution for codility challenge for parking bill calculator.
The solution provided is according to a set of instructions given by codility.
Function takes input as a time string for both enter and leaving time.
Function Call with sample input, parkingBillCalculator('10:00', '11:20')
