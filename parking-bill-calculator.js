
/* 
	My solution for codility challenge for parking bill calculator.
	The solution provided is according to a set of instructions given by codility.
	Function takes input as a time string for both enter and leaving time.
	Function Call with sample input, parkingBillCalculator('10:00', '11:20')
*/

function parkingBillCalculator(enterTime, leavingTime) {
	
	// convert time strings to seconds
	var startTime = new Date('1970-01-01T', enterTime + 'Z').getTime() / 1000;
	var endTime = new Date('1970-01-01T', leavingTime + 'Z').getTime() / 1000;

	// total time in terms of seconds
	var totalTime = endTime - startTime;

	// convert seconds to mintues
	var minutes = Math.floor(totalTime / 60);

	var entranceFee = 2;
	var firstHourRate = 3;
	var successiveHoursRate = 4;

	var basicCost = entranceFee + firstHourRate;
	var totalCost = 0;
	var excessTime = 0;

	// calculate total ticket price
	if (minutes > 60) {
		excessTime = minutes - 60;
		return totalCost = basicCost + (Math.ceil(excessTime/60)*successiveHoursRate);
	} else {
		return basicCost;
	}
}
// first argument is enter time, second argument is leaving time
// accepted time format is 'hh:mm'
parkingBillCalculator('10:00', "12:20");